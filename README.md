# Technical Test DD3
## Technologías
```
- Node.js
- Typescript
- PostgresSql
- Redis
- Jest
```

## Correr aplicación
```
Una vez clonado el repositorio:
1. Correr el comando "npm install"
2. Crear base de datos
3. Arrancar redis
4. Establecer variables de entorno
5. correr el comando: "npm run start"
```

## Variables de entorno (.env)
```
#Puerto del servidor
PORT=3000

#Conección a base de datos
DB_HOST=localhost
DB_PORT=5432
DB_USERNAME=
DB_PASSWORD=
DB_DATABASE=

#Conección a redis
DB_REDIS_URL=redis://localhost:6379

#Tamaño de palabras
WORD_SIZE=5

#Tiempo de partida
TIMER_IN_MILLISECONDS=300000

#Intentos máximos por palabra
PLAYER_ATTEMPTS=5

#Token para JWT
TOKEN_SECRET=208e8a5f-cbab-4e83-81a0-09909132e36d

#Url a diccionario de palabras
WORDS_API_URL=https://gitlab.com/d2945/words/-/raw/main/words.txt
```

## Correr test
```
npm run test
```

## Colección de postman
```
https://www.getpostman.com/collections/a8424935ab6ae2a86f58
```

## Observaciones
```
·Los servicios se encuentran protegidos y necesitan el header "access_token",
dicho token se puede obtener en los response headers del servicio "/play" con el cual el jugador iniciara la partida solo ingresando su nombre.
se recomienda copiar el token obtenido y pegarlo en las variables globales de la colección de postman para heredar en todos los servicios.

·La configuración del juego como los intentos por jugador, el tamaño de las palabras y el tiempo de partida se establecen desde variables de entorno,
los valores establecidos en el .env de inicio nos dicen que el jugador tendra 5 intentos, una duración de 5 minutos por partida y las palabras contendran 5 carácteres.

·El servicio "/top-words" tiene establecido un límite de 10 palabras por consulta,
pero puedes obtener menos o más palabras mandando el query param de "limit" con un número entero.

·Al arrancar la aplicación por primera vez se carga el diccionario de palabras, esto puede tardar unos segundos, una vez finalizado se muestra el siguiente mensaje en consola:
"Almacenamiento completo", durante el proceso se mostraran otros mensajes.
```