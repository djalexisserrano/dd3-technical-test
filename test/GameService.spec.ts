import axios from 'axios'
import { AppDataSource } from "../src/data-source"
import { Game } from "../src/entity/Game"
import { Player } from "../src/entity/Player"
import { Word } from "../src/entity/Word"
import { GameService } from "../src/services/GameService"
import { RedisService } from '../src/services/RedisService'
import { attemptResponse, currentGame, gameEntity, letterValidateResponse, playerEntity, word } from "../src/utils/mocks"

const gameService = new GameService()
const redisService = new RedisService()
const playerRepository = AppDataSource.getRepository(Player)
const wordRepository = AppDataSource.getRepository(Word)
const gameRepository = AppDataSource.getRepository(Game)

describe('test game service', () => {
    test('validate work', () => {
        const response = gameService.validateLetters('abran','arbal')
        const responseExpect = letterValidateResponse
        expect(response).toEqual(responseExpect)
    })
    test('test start game', async () => {
        jest.spyOn(playerRepository, 'save').mockResolvedValueOnce(playerEntity)
        const response = await gameService.play('christian')
        expect(response).toMatchObject(playerEntity)
    })
     test('test upsert game', async () => {
        jest.spyOn(gameRepository, 'save').mockResolvedValueOnce(gameEntity)
        const response:Game = await gameService.upsertGame(playerEntity, word, true, currentGame)
        expect(gameRepository.save).toBeCalled()
        expect(gameRepository.save).toBeCalledTimes(1)
        expect(response).toMatchObject(gameEntity)
    })
    test('test word dictionary storage', async () => {
        jest.spyOn(wordRepository, 'find').mockResolvedValueOnce([])
        jest.spyOn(wordRepository, 'save').mockResolvedValue(null)
        jest.spyOn(axios,'get').mockResolvedValueOnce(Promise.resolve({ data: 'ababilla\nababílla\nababillaba' }))
        await gameService.getAndInsertWordsInDB()
        expect(wordRepository.find).toBeCalled()
        expect(wordRepository.save).toBeCalled()
        expect(axios.get).toBeCalled()
    })
    test('test word selecction to play', async () => {
        jest.spyOn(wordRepository, 'query').mockResolvedValueOnce([word])
        jest.spyOn(wordRepository, 'update').mockResolvedValue(null)
        await gameService.setActiveWord()
        expect(wordRepository.query).toBeCalled()
        expect(wordRepository.update).toBeCalled()
    })
    test('test attempts', async () => {
        jest.spyOn(redisService, 'getRedis').mockResolvedValueOnce(JSON.stringify(word))
        jest.spyOn(AppDataSource.manager, 'findOneBy').mockResolvedValueOnce(currentGame)
        jest.spyOn(gameService, 'upsertGame').mockResolvedValueOnce(gameEntity)
        const response = await gameService.attempt(playerEntity, "abran")
        const responseExpect = attemptResponse
        expect(AppDataSource.manager.findOneBy).toBeCalled()
        expect(gameService.upsertGame).toBeCalled()
        expect(response).toEqual(responseExpect)
    })
})