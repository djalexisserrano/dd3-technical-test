import express, { Express } from 'express';
import dotenv from 'dotenv';
import "reflect-metadata"
import { AppDataSource } from './src/data-source';
import { GameService } from './src/services/GameService';
import apiRouter from './src/routes/api'

dotenv.config();

const app: Express = express();
const gameService = new GameService()
const { PORT, TIMER_IN_MILLISECONDS } = process.env

AppDataSource.initialize()
    .then(() => {
      init()
    })
    .catch((err) => {
        console.error("Error during Data Source initialization", err)
    })

const init = async () =>{
  await gameService.getAndInsertWordsInDB()
  gameService.setActiveWord()
  setActiveWordWithInterval()
}

const setActiveWordWithInterval = () => {
  setInterval(() => {
    gameService.setActiveWord()
  }, parseInt(TIMER_IN_MILLISECONDS))
};

app.use(express.json())
app.use(express.urlencoded({ extended: false }));
app.use('/', apiRouter);

app.listen(PORT, () => {
  console.log(`⚡️[server]: Server is running at https://localhost:${PORT}`);
});

//handler error
app.use((err, req, res, next) => {
  const { message, status } = err
  return res.status(status || 500).json({
      error: { message }
  })
})