import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm"
import { Game } from "./Game"

@Entity({ name: "words" })
export class Word {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    word: string

    @Column()
    was_played: boolean

    @OneToMany(() => Game, (game) => game.id_word)
    games?: Game[]
}