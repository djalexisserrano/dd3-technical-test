import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from "typeorm"
import { Word } from "./Word"
import { Player } from "./Player"

@Entity({ name: "games" })
export class Game {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    id_word: number

    @Column()
    id_player: number

    @Column()
    attempts: number

    @Column()
    is_won: boolean

    @ManyToOne(() => Word, (word) => word.id)
    @JoinColumn({ name: "id_word" })
    word?: Word

    @ManyToOne(() => Player, (player) => player.id)
    @JoinColumn({ name: "id_player" })
    player?: Player
}