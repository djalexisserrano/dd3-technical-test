import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm"
import { Game } from "./Game"

@Entity({ name: "players" })
export class Player {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @OneToMany(() => Game, (game) => game.id_word)
    games?: Game[]
}