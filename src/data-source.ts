import "reflect-metadata"
import dotenv from 'dotenv';
import { DataSource } from "typeorm"

dotenv.config();
const { DB_HOST, DB_PORT, DB_USERNAME, DB_PASSWORD, DB_DATABASE } = process.env

export const AppDataSource = new DataSource({
    type: "postgres",
    host: DB_HOST,
    port: parseInt(DB_PORT),
    username: DB_USERNAME,
    password: DB_PASSWORD,
    database: DB_DATABASE,
    logging: false,
    entities: ['dist/src/entity/**/*.js'],
    migrations: ['dist/src/migration/**/*.js'],
    subscribers: []
})