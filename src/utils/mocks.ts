import { Game } from "../entity/Game"
import { Player } from "../entity/Player"
import { Word } from "../entity/Word"

export const letterValidateResponse = [
    {
        "letter": "a",
        "value": 1
    },
    {
        "letter": "b",
        "value": 2
    },
    {
        "letter": "r",
        "value": 2
    },
    {
        "letter": "a",
        "value": 1
    },
    {
        "letter": "n",
        "value": 3
    }
]

export const attemptResponse = [
    {
        "letter": "a",
        "value": 1
    },
    {
        "letter": "b",
        "value": 1
    },
    {
        "letter": "r",
        "value": 1
    },
    {
        "letter": "a",
        "value": 1
    },
    {
        "letter": "n",
        "value": 1
    }
]

export const playerEntity:Player = {
    id: 1,
    name:'christian'
}

export const word:Word = {
    id: 1,
    word: 'abran',
    was_played: true
}

export const currentGame:Game = {
    id: 1,
    id_player: 1,
    id_word: 1,
    attempts: 1,
    is_won: false
}

export const currentGameWithMaxAttempts:Game = {
    id: 1,
    id_player: 1,
    id_word: 1,
    attempts: 5,
    is_won: false
}

export const gameEntity:Game = {
    id: 1,
    id_player: 1,
    id_word: 1,
    attempts: 2,
    is_won: true
}