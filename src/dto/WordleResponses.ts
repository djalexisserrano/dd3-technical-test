export type LetterValidateResponse = {
    letter: string,
    value: number
}

export type PlayerStatisticsResponse = {
    games_played: number,
    games_won: number
}

export type TopPlayerResponse = {
    name: string,
    games_won: number
}

export type TopWordResponse = {
    name: string
}