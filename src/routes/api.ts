import express, { Request, Response } from "express";
import jwt from 'jsonwebtoken'
import { Player } from "../entity/Player";
import verifyToken from "../middlewares/verifyToken";
import { LetterValidateResponse, TopPlayerResponse, PlayerStatisticsResponse, TopWordResponse } from "../dto/WordleResponses";
import { GameService } from '../services/GameService'
import { StatisticsService } from "../services/StatisticsService";
import wrap from '../utils/wrap'

const { WORD_SIZE, TOKEN_SECRET } = process.env
const router = express.Router();
const gameService = new GameService()
const statisticsService = new StatisticsService()

router.get('/', (req: Request, res: Response) => {
  res.send('Server Up!');
});

router.post('/play', wrap( async (req: Request, res: Response) => {
  const { name } = req.body
  if(!name.length){
    throw new Error('El nombre del jugador es requerido')
  }
  let playerCreated:Player = await gameService.play( name )
  const token = jwt.sign(playerCreated, TOKEN_SECRET)
  res.header('access_token', token).status(201).send()
}));

router.post('/attempt', verifyToken, wrap( async (req: Request, res: Response) => {
  const { user_word } = req.body
  const player: Player = req.player
  if(user_word.length != WORD_SIZE){
    throw new Error(`La palabra debe contener ${WORD_SIZE} carácteres`)
  }
  let attemptResponse:LetterValidateResponse[] = await gameService.attempt( player, user_word )
  res.send(attemptResponse);
}));

router.get('/player-statistics', verifyToken, wrap( async (req: Request, res: Response) => {
  const player: Player = req.player
  let playerStatisticsResponse:PlayerStatisticsResponse = await statisticsService.playerStatistics( player.id )
  res.send(playerStatisticsResponse);
}));

router.get('/topten-players', verifyToken, wrap( async (req: Request, res: Response) => {
  let topTenPlayersResponse:TopPlayerResponse[] = await statisticsService.topTenPlayers()
  res.send(topTenPlayersResponse);
}));

router.get('/top-words', verifyToken, wrap( async (req: Request, res: Response) => {
  const limit = req.query.limit ?? 10
  let topWordResponse:TopWordResponse[] = await statisticsService.topWords(limit)
  res.send(topWordResponse);
}));

export default router;