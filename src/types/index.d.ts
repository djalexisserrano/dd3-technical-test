import { Player } from "../entity/Player";

export {};

//se extiendee el type Request para agregar el atributo player que se agregara en el middleware de verificación
declare global {
  namespace Express {
    interface Request {
      player: Player;
    }
  }
}