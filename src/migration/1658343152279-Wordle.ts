import { MigrationInterface, QueryRunner, Table } from "typeorm"

export class Wordle1658343152279 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: "words",
                columns: [
                    {
                        name: "id",
                        type: "int",
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment'
                    },
                    {
                        name: "word",
                        type: "varchar",
                    },
                    {
                        name: "was_played",
                        type: "boolean",
                        default: false
                    },
                ]
            }),
            true
        )
        await queryRunner.createTable(
            new Table({
                name: "players",
                columns: [
                    {
                        name: "id",
                        type: "int",
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment'
                    },
                    {
                        name: "name",
                        type: "varchar",
                    },
                ]
            }),
            true
        )
        await queryRunner.createTable(
            new Table({
                name: "games",
                columns: [
                    {
                        name: "id",
                        type: "int",
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment'
                    },
                    {
                        name: "id_word",
                        type: "int",
                    },
                    {
                        name: "id_player",
                        type: "int",
                    },
                    {
                        name: "attempts",
                        type: "int",
                    },
                    {
                        name: "is_won",
                        type: "boolean",
                    },
                ],
                foreignKeys: [
                    {
                        columnNames: ['id_word'],
                        referencedTableName: 'words',
                        referencedColumnNames: ['id']
                    },
                    {
                        columnNames: ['id_player'],
                        referencedTableName: 'players',
                        referencedColumnNames: ['id']
                    }
                ]
            }),
            true
        )
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        const removeTables = [
            'words',
            'players',
            'games',
          ];
      
          removeTables.map(async (tableName) => {
            await queryRunner.query(`DROP TABLE "${tableName}" CASCADE`);
          });
    }

}
