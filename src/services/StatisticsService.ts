import { Repository } from 'typeorm'
import { AppDataSource } from '../data-source'
import { Word } from '../entity/Word'
import { Player } from '../entity/Player'
import { Game } from '../entity/Game'
import { TopPlayerResponse, PlayerStatisticsResponse, TopWordResponse } from '../dto/WordleResponses'

export class StatisticsService{

    private wordRepository:Repository<Word>
    private playerRepository:Repository<Player>
    private gameRepository:Repository<Game>

    constructor(){
        this.wordRepository = AppDataSource.getRepository(Word)
        this.playerRepository = AppDataSource.getRepository(Player)
        this.gameRepository = AppDataSource.getRepository(Game)
    }

    async playerStatistics( playerId: number ):Promise<PlayerStatisticsResponse> {
        const gamesPlayed = await this.gameRepository.count({
            where:{ id_player: playerId }
        })
        const gamesWon = await this.gameRepository.count({
            where:{ id_player: playerId, is_won: true }   
        })
        const playerStaticResponse:PlayerStatisticsResponse = {
            games_played: gamesPlayed,
            games_won: gamesWon
        }
        return playerStaticResponse
    }

    async topTenPlayers():Promise<TopPlayerResponse[]> {
        let topTenPlayersResponse = await this.playerRepository.query(`
        SELECT name,games_won
            FROM players JOIN (
                SELECT id_player, count(games.id) games_won
                FROM games WHERE is_won = TRUE
                GROUP BY id_player limit 10)
            AS gw ON players.id = gw.id_player
            order by games_won desc`)
        return topTenPlayersResponse
    }

    async topWords(limit):Promise<TopWordResponse[]> {
        let topWordResponse = await this.wordRepository.query(`
        SELECT word,games_won
            FROM words JOIN (
		        SELECT id_word, count(games.id) games_won
		        FROM games WHERE is_won = TRUE
                GROUP BY id_word limit $1)
            AS tw ON words.id = tw.id_word
            order by games_won desc;
        `,[limit])
        return topWordResponse
    }
}
