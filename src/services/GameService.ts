import axios from 'axios'
import { Repository } from 'typeorm'
import { AppDataSource } from '../data-source'
import { Word } from '../entity/Word'
import { Player } from '../entity/Player'
import { Game } from '../entity/Game'
import { RedisService } from './RedisService'
import { LetterValidateResponse, PlayerStatisticsResponse } from '../dto/WordleResponses'

const { WORDS_API_URL, WORD_SIZE, PLAYER_ATTEMPTS } = process.env

export class GameService{

    private wordRepository:Repository<Word>
    private playerRepository:Repository<Player>
    private gameRepository:Repository<Game>
    private redisService: RedisService

    constructor(){
        this.wordRepository = AppDataSource.getRepository(Word)
        this.playerRepository = AppDataSource.getRepository(Player)
        this.gameRepository = AppDataSource.getRepository(Game)
        this.redisService = new RedisService()
    }

    async getAndInsertWordsInDB(){
        const words_exist = await this.wordRepository.find({ take:1 })
        if( !words_exist.length ){
            console.log("Obteniendo diccionario de datos");
            const wordsString = (await axios.get( WORDS_API_URL )).data
            const words = wordsString.split("\n").map( word => {
                return { word }
            })
            console.log("Almacenando diccionario de datos");
            await this.wordRepository.save(words,{ chunk: 10000 })
            console.log("Almacenamiento completo");
        }
    }

    async setActiveWord(){
        let [ randomWord ] = await this.wordRepository.query(
            'select * from words where LENGTH(word) = $1 and was_played != true order by random() limit 1;',
            [WORD_SIZE]
        )
        await this.wordRepository.update(randomWord.id,{
            was_played: true
        })
        this.redisService.setRedis('active_word', JSON.stringify(randomWord))
    }

    async play( name: string ): Promise<Player>{
        let playerCreated: Player = await this.playerRepository.save({ name })
        return playerCreated
    }

    async attempt( player:Player, user_word: string ): Promise<LetterValidateResponse[]>{
        const activeWordEntity:Word = JSON.parse(await this.redisService.getRedis('active_word'))
        const currentGame:Game = await AppDataSource.manager.findOneBy(Game,{
            id_player: player.id,
            id_word: activeWordEntity.id
        })
        this.validateMaxAttempts(currentGame)
        const attemptResponse:LetterValidateResponse[] = this.validateLetters(user_word, activeWordEntity.word)
        const gameIsWon:boolean = attemptResponse.every( letter => letter.value == 1)
        await this.upsertGame(player, activeWordEntity, gameIsWon, currentGame)
        return attemptResponse
    }

    validateMaxAttempts(currentGame:Game){
        if(currentGame && currentGame.attempts >= parseInt(PLAYER_ATTEMPTS)){
            throw new Error(`El jugador llego al límite de intentos (${PLAYER_ATTEMPTS})`)
        }
    }

    validateLetters( user_word:string, active_word:string ):LetterValidateResponse[]{
        const userWordArray = [...user_word]
        const activeWordArray = [...active_word]
        const attemptResponse = userWordArray.map( (letter, index) => {
            if( userWordArray[index] == activeWordArray[index] ){
                return { letter, value:1 }
            }
            if( activeWordArray.includes(letter) ){
                return { letter, value:2 }
            }else{
                return { letter, value:3 }
            }
        })
        return attemptResponse
    }

    async upsertGame(player:Player, word:Word, gameIsWon:boolean, currentGame:Game):Promise<Game>{
         const gameUpdate = {
            id: currentGame? currentGame.id : null,
            id_player: player.id,
            id_word: word.id,
            attempts: currentGame? currentGame.attempts+1 : 1,
            is_won: gameIsWon
        }
        return await this.gameRepository.save(gameUpdate)
    }
}
