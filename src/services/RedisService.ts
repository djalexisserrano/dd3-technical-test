import { createClient } from 'redis'
import { promisify } from 'util'

const { DB_REDIS_URL:url } = process.env

export class RedisService{

    private client

    constructor(){
        this.client = createClient({ url })

        this.client.on('error', err => {
            console.log('Something went wrong ' + err);
        });
    }

    async getRedis(key){
      const getAsync = promisify(this.client.get).bind(this.client);
      return await getAsync(key);
    }

    setRedis(key, value){
        this.client.set(key, value)
    }

}