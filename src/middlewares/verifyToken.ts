import jwt from 'jsonwebtoken'
import { Player } from '../entity/Player'
const { TOKEN_SECRET } = process.env

const verifyToken = (req, res, next) => {
    const token = req.header('access_token')
    if (!token) return res.status(401).json({ error: 'Acceso denegado' })
    try {
        const verified: Player = jwt.verify(token, TOKEN_SECRET)
        req.player = verified
        next()
    } catch (error) {
        res.status(400).json({error: 'token no es válido'})
    }
}

export = verifyToken